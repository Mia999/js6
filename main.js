
function generalizedFibonacci(F0, F1, n) {
    if (n === 0) {
        return F0;
    } else if (n === 1) {
        return F1;
    } else {
        let prevPrev = F0;
        let prev = F1;
        let current;

        for (let i = 2; i <= Math.abs(n); i++) {
            current = prevPrev + prev;
            prevPrev = prev;
            prev = current;
        }

        return n > 0 ? current : n % 2 === 0 ? current : -current;
    }
}

// ввод пользователя через модальное окно
const n = parseInt(prompt("Введите номер n:"));
const result = generalizedFibonacci(0, 1, n);
alert(`n-ое обобщенное число Фибоначчи: ${result}`);



